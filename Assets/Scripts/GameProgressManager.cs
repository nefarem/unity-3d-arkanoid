﻿using UnityEngine;

public class GameProgressManager : MonoBehaviour
{
    public int currentLevel; // Aktualny poziom
    public int levelToContinue; // Następny poziom
    public int highscore;  // Najwyższy wynik
    public int score; // Punkty
    public int life = 3; // Ilość żyć

    void Update()
    {
        switch (currentLevel)
        {
            case 1:
                levelToContinue = 2;
                break;

            case 2:
                levelToContinue = 3;
                break;

            case 3:
                levelToContinue = 4;
                break;

            case 4:
                levelToContinue = 5;
                break;

            case 5:
                levelToContinue = 5;
                break;

            case 6:
                levelToContinue = 5;
                break;

        }
    }

}
