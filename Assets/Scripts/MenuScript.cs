﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public GameObject continueButton;

    GameProgressManager gameProgressManager;

    string gamePath;
    string initVector = "pemgail9uzpgzl88";
    int keysize = 256;

    void Start()
    {
        gameProgressManager = GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>();
        gamePath = Application.dataPath;

        Time.timeScale = 1f;

        // Gdy gra zostanie uruchomiona, zostanie sprawdzone czy istnieje plik z ostatnim zapisem gry, jeśli tak zostanie on załadowany i pobrane z niego zostaną dane.

        Directory.CreateDirectory(gamePath + @"\GameSave");

        string saveFilePath = gamePath + @"\GameSave\Save.sav";

        if (File.Exists(saveFilePath))
        {
            continueButton.SetActive(true);

            string line = "";

            using (StreamReader sr = new StreamReader(saveFilePath))
            {
                line = sr.ReadLine();
                string decrypt = DecryptString(line, "lazydog");

                string[] splitLine = decrypt.Split('|');

                // Wczytanie aktualnego poziomu na którym był ostatni zapis oraz najwyższy wynik.
                gameProgressManager.currentLevel = Convert.ToInt32(splitLine[splitLine.Length - 1]);
                gameProgressManager.highscore = Convert.ToInt32(splitLine[splitLine.Length - 2]);
            }
        }
        // Gdy takowego pliku nie ma, zostanie utworzony
        else
        {
            continueButton.SetActive(false);

            File.Create(saveFilePath);

            GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>().currentLevel = 1;
        }
    }

    // Funkcje wywoływane przez kliknięcie myszką.
    public void NewGame()
    {
        DontDestroyOnLoad(GameObject.Find("GameProgressManager"));
        gameProgressManager.highscore = 0;
        gameProgressManager.score = 0;
        gameProgressManager.currentLevel = 1;


        SceneManager.LoadScene("Level" + gameProgressManager.currentLevel);
    }


    public void ContinueGame()
    {
        DontDestroyOnLoad(GameObject.Find("GameProgressManager"));

        SceneManager.LoadScene("ContinueScene");
    }

    public void MultiplayerGame()
    {
        SceneManager.LoadScene("MultiLobby");
    }

    public void ExitGame()
    {
        Application.Quit();
    }


    string DecryptString(string cipherText, string passPhrase)
    {
        byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
        byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
        PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
        byte[] keyBytes = password.GetBytes(keysize / 8);
        RijndaelManaged symmetricKey = new RijndaelManaged();
        symmetricKey.Mode = CipherMode.CBC;
        ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
        MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
        CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
        byte[] plainTextBytes = new byte[cipherTextBytes.Length];
        int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        memoryStream.Close();
        cryptoStream.Close();
        return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
    }

}
