﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapGenerator : MonoBehaviour
{

    // Generowanie plansz
    // Utworzyłem kilka szablonów map które są generowane na poszczególnych poziomach.

    public Sprite[] brickSprites;

    public GameObject brickPrfab;

    public enum MapTemplate { Classic, Crossed, ClassicIndestructibleWall, Road, Pyramide}
    public MapTemplate mapTemplate;

    GameManager gameManager;

    void Start()
    {
        if (SceneManager.GetActiveScene().name != "ContinueScene") GenerateMap();

        gameManager = GetComponent<GameManager>();

        gameManager.bricks = GameObject.FindGameObjectsWithTag("Brick");
        gameManager.balls = GameObject.FindGameObjectsWithTag("Ball");

        gameManager.bricksCounter = gameManager.bricks.Length;
        gameManager.ballsCounter = gameManager.balls.Length;
    }

    void GenerateMap()
    {
        if (mapTemplate == MapTemplate.Classic)
        {
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 6; y++)
                {
                    GameObject brick = Instantiate(brickPrfab, new Vector2(-3.1823f + 0.7047f * x, 3.31f - 0.3f * y), Quaternion.identity);

                    brick.GetComponent<Brick>().life = 1;

                    int rnd = Random.Range(0, 5);

                    if (rnd == 3) brick.GetComponent<Brick>().havePowerUp = true;

                    switch (y)
                    {
                        case 0:
                            brick.GetComponent<SpriteRenderer>().sprite = brickSprites[0];
                            brick.GetComponent<Brick>().spriteIndex = 0;
                            break;

                        case 1:
                            brick.GetComponent<SpriteRenderer>().sprite = brickSprites[2];
                            brick.GetComponent<Brick>().spriteIndex = 2;
                            break;

                        case 2:
                            brick.GetComponent<SpriteRenderer>().sprite = brickSprites[4];
                            brick.GetComponent<Brick>().spriteIndex = 4;
                            break;

                        case 3:
                            brick.GetComponent<SpriteRenderer>().sprite = brickSprites[6];
                            brick.GetComponent<Brick>().spriteIndex = 6;
                            break;

                        case 4:
                            brick.GetComponent<SpriteRenderer>().sprite = brickSprites[8];
                            brick.GetComponent<Brick>().spriteIndex = 8;
                            break;

                    }
                }
            }
        }

        if (mapTemplate == MapTemplate.Crossed)
        {
            for (int x = 0; x < 10; x++)
            {
                GameObject brick = Instantiate(brickPrfab, new Vector2(-3.1823f + 0.7047f * x, 3.31f - 0.3f * x), Quaternion.identity);

                brick.GetComponent<Brick>().life = 2;

                int rnd = Random.Range(0, 5);

                if (rnd == 4) brick.GetComponent<Brick>().havePowerUp = true;

                brick.GetComponent<SpriteRenderer>().sprite = brickSprites[10];
                brick.GetComponent<Brick>().spriteIndex = 10;
                brick.GetComponent<Brick>().damageBrickSprite = brickSprites[11];
            }

            for (int x = 0; x < 10; x++)
            {
                GameObject brick = Instantiate(brickPrfab, new Vector2(3.16f - 0.7047f * x, 3.31f - 0.3f * x), Quaternion.identity);

                brick.GetComponent<Brick>().life = 2;

                int rnd = Random.Range(0, 5);

                if (rnd == 4) brick.GetComponent<Brick>().havePowerUp = true;

                brick.GetComponent<SpriteRenderer>().sprite = brickSprites[12];
                brick.GetComponent<Brick>().spriteIndex = 12;
                brick.GetComponent<Brick>().damageBrickSprite = brickSprites[13];
            }

            for (int x = 0; x < 10; x++)
            {
                if (x != 4 && x != 5)
                {
                    GameObject brick = Instantiate(brickPrfab, new Vector2(0.3411999f, 3.31f - 0.3f * x), Quaternion.identity);

                    brick.GetComponent<Brick>().life = 2;

                    int rnd = Random.Range(0, 5);

                    if (rnd == 4) brick.GetComponent<Brick>().havePowerUp = true;

                    brick.GetComponent<SpriteRenderer>().sprite = brickSprites[14];
                    brick.GetComponent<Brick>().spriteIndex = 14;
                    brick.GetComponent<Brick>().damageBrickSprite = brickSprites[15];
                }
            }

            for (int x = 0; x < 10; x++)
            {
                if (x != 4 && x != 5)
                {
                    GameObject brick = Instantiate(brickPrfab, new Vector2(-3.1823f + 0.7047f * x, 2.11f), Quaternion.identity);

                    brick.GetComponent<Brick>().life = 2;

                    int rnd = Random.Range(0, 5);

                    if (rnd == 4) brick.GetComponent<Brick>().havePowerUp = true;

                    brick.GetComponent<SpriteRenderer>().sprite = brickSprites[16];
                    brick.GetComponent<Brick>().spriteIndex = 16;
                    brick.GetComponent<Brick>().damageBrickSprite = brickSprites[17];
                }

            }
        }

        if (mapTemplate == MapTemplate.ClassicIndestructibleWall)
        {
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    GameObject brick = Instantiate(brickPrfab, new Vector2(-3.1823f + 0.7047f * x, 3.31f - 0.3f * y), Quaternion.identity);

                    brick.GetComponent<Brick>().life = Random.Range(1, 3);

                    int rnd = Random.Range(0, 5);

                    if (rnd == 3) brick.GetComponent<Brick>().havePowerUp = true;

                    while (rnd % 2 != 0) rnd = Random.Range(0, brickSprites.Length);

                    brick.GetComponent<SpriteRenderer>().sprite = brickSprites[rnd];
                    brick.GetComponent<Brick>().spriteIndex = rnd;
                    brick.GetComponent<Brick>().damageBrickSprite = brickSprites[rnd + 1];
                }
            }

            for (int x = 0; x < 10; x++)
            {
                if (x != 3 && x != 4 && x != 5 && x != 6)
                {
                    GameObject brick = Instantiate(brickPrfab, new Vector2(-3.1823f + 0.7047f * x, 2.11f), Quaternion.identity);

                    brick.GetComponent<Brick>().life = 3;

                    brick.GetComponent<SpriteRenderer>().sprite = brickSprites[16];
                    brick.GetComponent<Brick>().spriteIndex = 16;
                    brick.gameObject.tag = "BrickIndestructible";
                }

            }
        }

        if (mapTemplate == MapTemplate.Road)
        {
            GameObject brick1 = Instantiate(brickPrfab, new Vector2(-3.1823f, 3.31f), Quaternion.identity);
            GameObject brick2 = Instantiate(brickPrfab, new Vector2(-3.1823f, 3.01f), Quaternion.identity);

            brick1.GetComponent<Brick>().life = 1;
            brick2.GetComponent<Brick>().life = 2;

            int rnd = Random.Range(0, 5);

            while (rnd % 2 != 0) rnd = Random.Range(0, brickSprites.Length);

            brick1.GetComponent<SpriteRenderer>().sprite = brickSprites[rnd];
            brick1.GetComponent<Brick>().spriteIndex = rnd;
            brick1.GetComponent<Brick>().damageBrickSprite = brickSprites[rnd + 1];

            brick2.GetComponent<SpriteRenderer>().sprite = brickSprites[rnd];
            brick2.GetComponent<Brick>().spriteIndex = rnd;
            brick2.GetComponent<Brick>().damageBrickSprite = brickSprites[rnd + 1];

            for (int x = 0; x < 9; x++)
            {
                GameObject brick = Instantiate(brickPrfab, new Vector2(-3.1823f + 0.7047f * x, 2.71f), Quaternion.identity);

                brick.GetComponent<Brick>().life = 3;
                brick.gameObject.tag = "BrickIndestructible";

                brick.GetComponent<SpriteRenderer>().sprite = brickSprites[16];
                brick.GetComponent<Brick>().spriteIndex = 16;
                brick.GetComponent<Brick>().damageBrickSprite = brickSprites[16 + 1];
            }

            for (int x = 0; x < 9; x++)
            {
                GameObject brick = Instantiate(brickPrfab, new Vector2(-2.4776f + 0.7047f * x, 1.81f), Quaternion.identity);

                brick.GetComponent<Brick>().life = 3;

                brick.GetComponent<SpriteRenderer>().sprite = brickSprites[16];
                brick.GetComponent<Brick>().spriteIndex = 16;
                brick.gameObject.tag = "BrickIndestructible";

            }

            for (int x = 0; x < 9; x++)
            {
                GameObject brick = Instantiate(brickPrfab, new Vector2(-3.1823f + 0.7047f * x, 0.9099998f), Quaternion.identity);

                brick.GetComponent<Brick>().life = 3;

                brick.GetComponent<SpriteRenderer>().sprite = brickSprites[16];
                brick.GetComponent<Brick>().spriteIndex = 16;
                brick.gameObject.tag = "BrickIndestructible";

            }
        }


        if (mapTemplate == MapTemplate.Pyramide)
        {
            int z = 0;

            for (int y = 0; y < 6; y++)
            {
                for (int x = 0; x < 10 - (2 * z); x++)
                {
                    GameObject brick = Instantiate(brickPrfab, new Vector2(-3.1823f + 0.7047f * x, 3.31f - 0.3f * y), Quaternion.identity);

                    brick.GetComponent<Brick>().life = Random.Range(1,3);

                    int rnd = Random.Range(0, 5);

                    if (rnd == 3) brick.GetComponent<Brick>().havePowerUp = true;

                    while (rnd % 2 != 0) rnd = Random.Range(0, brickSprites.Length);

                    brick.GetComponent<SpriteRenderer>().sprite = brickSprites[rnd];
                    brick.GetComponent<Brick>().spriteIndex = rnd;
                    brick.GetComponent<Brick>().damageBrickSprite = brickSprites[rnd + 1];
                }
    
                z++;
            }
        }

    }

}
