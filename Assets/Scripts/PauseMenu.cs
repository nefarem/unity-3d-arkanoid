﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    // Powrót do menu gdy w menu pauzy gracz kliknie przycisk z powrotem.
    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
