﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Networking;

public class Ball : MonoBehaviour
{
    public GameObject powerUpPrefab;
    public GameObject brickHitEffect;

    public AudioClip brickHit;
    public AudioClip ballBounce;

    public string moveDirection; //Kierunki ruchu piłki: [LeftUp, LeftDown, RightUp, RightDown]

    GameManager gameManager;
    AudioSource audioSource;

    float speed = 0.1f;

    bool isFall; // Określa czy piła spada czy nie

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        audioSource = GetComponent<AudioSource>();

        moveDirection = "RightUp";
        isFall = false;
    }

    void Update()
    {
        // Poruszanie piłką jeżeli gra wystartowała
        if (gameManager.gameStarted) MoveBall();
    }

    void MoveBall()
    {
        // W zależności od aktualnego kierunku w zmiennej "moveDirection" tak piłka będzie przemieszczana na mapie.
        switch (moveDirection)
        {
            case "LeftUp":
                transform.position += new Vector3(-25f * speed * Time.deltaTime, 25f * speed * Time.deltaTime);
                break;

            case "LeftDown":
                transform.position += new Vector3(-25f * speed * Time.deltaTime, -25f * speed * Time.deltaTime);
                break;

            case "RightUp":
                transform.position += new Vector3(25f * speed * Time.deltaTime, 25f * speed * Time.deltaTime);
                break;

            case "RightDown":
                transform.position += new Vector3(25f * speed * Time.deltaTime, -25f * speed * Time.deltaTime);
                break;
        }
    }

    void HitWall(GameObject collision)
    {
        audioSource.clip = ballBounce;
        audioSource.Play();


        // Kierunek piłki jest zmieniony w zależności od tego w którą scianę uderzyła
        if (collision.GetComponent<Wall>().wallEnum == Wall.wall.left)
        {
            if (!isFall) moveDirection = "RightUp";
            else moveDirection = "RightDown";
        }

        if (collision.GetComponent<Wall>().wallEnum == Wall.wall.right)
        {
            if (!isFall) moveDirection = "LeftUp";
            else moveDirection = "LeftDown";
        }

        if (collision.GetComponent<Wall>().wallEnum == Wall.wall.up)
        {
            isFall = true;

            if (moveDirection == "LeftUp") moveDirection = "LeftDown";
            else moveDirection = "RightDown";
        }

        // Jeżeli piłka wypadnie po za mapę zostanie usunięta a z licznika piłek odjęte będzie 1.
        if (collision.GetComponent<Wall>().wallEnum == Wall.wall.down)
        {
            Destroy(gameObject);

            gameManager.ballsCounter--;
        }
    }

    void HitPlatform(GameObject collision)
    {
        audioSource.clip = ballBounce;
        audioSource.Play();

        isFall = false;

        // Jeżeli piłka uderzy w lewą połowę platformy zostanie skierowana w kierunek lewo - góra, natomiast w przypadku uderzenia w prawą połowę, uda się w prawo - góra.
        if (collision.name == "LeftHalfCollision") moveDirection = "LeftUp";
        else moveDirection = "RightUp";
    }

    void HitBrick(GameObject collision)
    {
        audioSource.clip = brickHit;
        audioSource.Play();

        // Efekt cząsteczkowy
        Instantiate(brickHitEffect, collision.gameObject.transform.position, Quaternion.identity);

        // Jeżeli piłka jest nad klockiem wtedy zostanie skierowana w górę, w przeciwnym razie obierze kierunek w dół.
        if (gameObject.transform.position.y > collision.transform.position.y)
        {
            isFall = false;

            if (moveDirection == "LeftDown") moveDirection = "RightUp";
            else moveDirection = "LeftUp";
        }
        else
        {
            isFall = true;

            if (moveDirection == "LeftUp") moveDirection = "RightDown";
            else moveDirection = "LeftDown";
        }

        // Gdy klocek ma 1 hp
        if (collision.GetComponent<Brick>().life == 1)
        {
            // Sprawdzenie czy posiada PowerUp, jeśli tak zostanie on zrespiony w miejsce piłki wraz z losowym efektem.
            // Jeżeli zostanie wylosowany 'czasowy' bonus (działka, powiększenie/zmniejszenie platformy, spowolnienie/przyspieszenie czasu) wtedy kolejny taki bonus nie będzie mógł się zrespić do czasu gdy nie skończy się aktualny (jeżeli gracz go podniesie) lub nie wyleci po za mapę.
            if (collision.gameObject.GetComponent<Brick>().havePowerUp)
            {
                GameObject powerUp = Instantiate(powerUpPrefab, collision.transform.position, Quaternion.identity);

                int rnd = Random.Range(0, 10);

                if (gameManager.spawnTimePowerUp == true) rnd = Random.Range(0, 5);

                if (rnd >= 5) gameManager.spawnTimePowerUp = true;

                powerUp.GetComponent<PowerUp>().powerUpType = (PowerUp.PowerUpEnum)rnd;
                powerUp.GetComponent<SpriteRenderer>().sprite = powerUp.GetComponent<PowerUp>().powerUpSprites[rnd];
            }

            Destroy(collision.gameObject);
            gameManager.score += 10;
            gameManager.bricksCounter--;
        }
        // Gdy klocek ma 2 hp najpierw zostanie mu jedno odebrane a następnie grafika zmieniona na "uszkodzoną"
        else if (collision.GetComponent<Brick>().life == 2)
        {
            collision.GetComponent<Brick>().life--;
            collision.GetComponent<Brick>().SetDamageSprite();
        }
    }

    // Niezniszczalny klocek
    void HitBrickIndestructible(GameObject collision)
    {
        audioSource.clip = brickHit;
        audioSource.Play();

        // Jeżeli piłka jest nad klockiem wtedy zostanie skierowana w górę, w przeciwnym razie obierze kierunek w dół.
        if (gameObject.transform.position.y > collision.transform.position.y)
        {
            isFall = false;

            if (moveDirection == "LeftDown") moveDirection = "RightUp";
            else moveDirection = "LeftUp";
        }
        else
        {
            isFall = true;

            if (moveDirection == "LeftUp") moveDirection = "RightDown";
            else moveDirection = "LeftDown";
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Uderzenie piłki w ścianę
        if (collision.gameObject.CompareTag("Wall")) HitWall(collision.gameObject);

        // Uderzenie piłki w platformę
        if (collision.gameObject.CompareTag("Platform")) HitPlatform(collision.gameObject);

        // Uderzenie piłki w klocek
        if (collision.gameObject.CompareTag("Brick")) HitBrick(collision.gameObject);

        // Uderzenie piłki w niezniszczalny klocek
        if (collision.gameObject.CompareTag("BrickIndestructible")) HitBrickIndestructible(collision.gameObject);

    }

}
