﻿using System.Security.Cryptography;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    // Dostępne typy bonusów
    public enum PowerUpEnum { Plus50, Plus100, Plus250, Plus500, MultiBalls, Slow, Fast, Gun, SmallPlatform, BigPlatform}
    public PowerUpEnum powerUpType;

    public AudioClip powerUpPickUp;

    public GameObject bigPlatformPrefab;
    public GameObject smallPlatformPrefab;
    public GameObject ballPrefab;

    public Sprite[] powerUpSprites;

    AudioSource audioSource;
    GameManager gameManager;

    float speed = 0.6f;

    bool isPickUp = false;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        // Przesuwanie power upa w dół
        transform.position += new Vector3(0, -1f * speed * Time.deltaTime);

        if (!audioSource.isPlaying && isPickUp) Destroy(gameObject);
    }

    // Gdy powerUp wyleci po za mapę to zostanie usunięty.
    // Dodatkowo jeżeli był to czasowy wtedy gameManager.spawnTimePowerUp = false; co umożliwi szansę na pojawienie się kolejnego czasowego ulepszenia.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Down") Destroy(gameObject);

        if (collision.gameObject.CompareTag("Wall"))
        {
            if (collision.GetComponent<Wall>().wallEnum == Wall.wall.down)
            {
                Destroy(gameObject);

                if (gameManager.spawnTimePowerUp) gameManager.spawnTimePowerUp = false;
            }
        }

        // Gdy power up zostanie podniesiony przez gracza, otrzyma on bonus w zależności od tego jaki to był power up.

        if (collision.gameObject.tag == "Platform")
        {

            audioSource.clip = powerUpPickUp;
            audioSource.Play();

            // Dodatkowe punkty
            if (powerUpType == PowerUpEnum.Plus50) gameManager.score += 50;
            if (powerUpType == PowerUpEnum.Plus100) gameManager.score += 100;
            if (powerUpType == PowerUpEnum.Plus250) gameManager.score += 250;
            if (powerUpType == PowerUpEnum.Plus500) gameManager.score += 500;

            // Spowolnienie czasu
            if (powerUpType == PowerUpEnum.Slow)
            {
                Time.timeScale = 0.4f;
                gameManager.startTimer = true;
            }


            // Przyspieszenie czasu
            if (powerUpType == PowerUpEnum.Fast)
            {
                Time.timeScale = 1.4f;
                gameManager.startTimer = true;
            }

            // Dodatkowe piłki
            if (powerUpType == PowerUpEnum.MultiBalls)
            {
                for (int i = 0; i < Random.Range(1, 3); i++)
                {
                    Instantiate(ballPrefab, new Vector2(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y + 2f), Quaternion.identity);
                    gameManager.ballsCounter++;
                }            
            }

            // Działko
            if (powerUpType == PowerUpEnum.Gun)
            {
               // collision.gameObject.transform.parent.gameObject.GetComponent<Animator>().runtimeAnimatorController = gunPlatformAnimController;

                collision.gameObject.transform.parent.gameObject.GetComponent<Platform>().canShot = true;
                collision.gameObject.transform.parent.gameObject.GetComponent<Animator>().Play("PlatformGuns");
                gameManager.startTimer = true;
            }

            // Mała platforma
            if (powerUpType == PowerUpEnum.SmallPlatform)
            {
                Instantiate(smallPlatformPrefab, collision.gameObject.transform.parent.gameObject.transform.position, Quaternion.identity);
                Destroy(collision.gameObject.transform.parent.gameObject);
                gameManager.startTimer = true;
            }

            // Duża platforma
            if (powerUpType == PowerUpEnum.BigPlatform)
            {
                Instantiate(bigPlatformPrefab, collision.gameObject.transform.parent.gameObject.transform.position, Quaternion.identity);
                Destroy(collision.gameObject.transform.parent.gameObject);
                gameManager.startTimer = true;
            }

            isPickUp = true;
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;

        }
    }
}
