﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public int life;  // 1, 2 - zniszczalne, 3 - niezniszczalne

    public bool havePowerUp; // Czy posiada power upa czy nie.

    [HideInInspector]
    public int spriteIndex; // Index sprite'a z tablicy [brickSprites] w MapGenerator.cs potrzebnego do zapisu/wczytania gry.

    public Sprite damageBrickSprite; // Sprite uszkodzonego klocka.

    public void SetDamageSprite()
    {
        GetComponent<SpriteRenderer>().sprite = damageBrickSprite;
    }
}
