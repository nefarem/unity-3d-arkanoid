﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndLevelUI : MonoBehaviour
{
    // Napisy w UI
    public Text resultLevelText;
    public Text resultLevelButtonText;

    GameManager gameManager;
    GameProgressManager gameProgressManager;

    bool isWin;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameProgressManager = GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>();

        // Jeżeli w grze zostało 0 klocków możliwych do zniszczenia oraz liczba piłek jest większa od 0 wtedy napis zmieni się na Wygraną
        // W przeciwnym wypadku wyświetlony zostanie napis o przegranej.
        if (gameManager.bricksCounter == 0 && gameManager.ballsCounter > 0)
        {
            isWin = true;

            resultLevelText.text = "Wygrałeś";
            resultLevelButtonText.text = "Następny poziom";

            if (SceneManager.GetActiveScene().name == "Level5")
            {
                resultLevelText.text = "Ukończyłeś wszystkie dostępne plansze";
                resultLevelButtonText.text = "Powtórz ostatni poziom";
            }
        }

        if (gameManager.ballsCounter == 0 && gameManager.bricksCounter > 0)
        {
            isWin = false;

            if (GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>().life > 0)
            {
                resultLevelText.text = "Przegrałeś";
                resultLevelButtonText.text = "Powtórz poziom";
            }
            else
            {
                resultLevelText.text = "Przegrałeś - straciłeś wszystkie życia";
                resultLevelButtonText.text = "Powrót do menu";
            }


        }
    }

    // Funkcja wywoływana podczas naciśniecią przycisku w oknie zakończenia levelu (przegrana albo wygrana)
    public void RestartNextLevel()
    {
        // Jeżeli gracz nie wygrał, przycisk sprawi że poziom zostanie zresetowany.
        if (!isWin)
        {
            if (GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>().life > 0)
            {
                GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>().life--;
                DontDestroyOnLoad(GameObject.Find("GameProgressManager"));

                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            else
            {
                GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>().score = 0;
                GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>().highscore = 0;
                GameObject.Find("GameProgressManager").GetComponent<GameProgressManager>().life = 3;

                SceneManager.LoadScene("Menu");
            }

  
        }
        // W przeciwnym wypadku zostanie uruchomiona nowa plansza.
        else
        {
            DontDestroyOnLoad(GameObject.Find("GameProgressManager"));

            if (gameProgressManager.currentLevel <= 5)
            {
                SceneManager.LoadScene("Level" + Convert.ToInt32(gameProgressManager.currentLevel + 1));
                gameProgressManager.currentLevel++;
            }
                
        }
    }

    // Funkcja wywoływana podczas naciśniecią przycisku w oknie zakończenia levelu (przegrana albo wygrana) powracająca do menu
    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }


}
