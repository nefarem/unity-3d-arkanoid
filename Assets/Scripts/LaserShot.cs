﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserShot : MonoBehaviour
{
    // Laserowy pocisk wystrzeliwany z platformy gdy gracz zdobędzie PowerUpa.

    public AudioClip laserHit;

    public GameObject powerUpPrefab;

    AudioSource audioSource;
    GameManager gameManager;

    float speed = 3f;

    bool hit;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        transform.position += new Vector3(0, 1f * speed * Time.deltaTime);

        // Usuniecie przycisku gdy skończy się dźwięk
        if (hit && audioSource.isPlaying == false) Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {

        // Gdy pocisk uderzy w klocek

        if (collision.gameObject.CompareTag("Brick"))
        {
            audioSource.clip = laserHit;
            audioSource.Play();

            hit = true;

            // Tymczasowe ukrycie pocisku ale nie usuniecie z mapy ponieważ w momencie wywołania  Destroy() nie odtwarzał się dźwięk.
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;

            // Sprawdzenie czy posiada PowerUp, jeśli tak zostanie on zrespiony w miejsce piłki wraz z losowym efektem.
            // Jeżeli zostanie wylosowany 'czasowy' bonus (działka, powiększenie/zmniejszenie platformy, spowolnienie/przyspieszenie czasu) wtedy kolejny taki bonus nie będzie mógł się zrespić do czasu gdy nie skończy się aktualny (jeżeli gracz go podniesie) lub nie wyleci po za mapę.
            if (collision.gameObject.GetComponent<Brick>().havePowerUp)
            {
                GameObject powerUp = Instantiate(powerUpPrefab, collision.transform.position, Quaternion.identity);

                int rnd = Random.Range(0, 10);

                if (gameManager.spawnTimePowerUp == true) rnd = Random.Range(0, 5);

                if (rnd >= 5) gameManager.spawnTimePowerUp = true;

                powerUp.GetComponent<PowerUp>().powerUpType = (PowerUp.PowerUpEnum)rnd;
                powerUp.GetComponent<SpriteRenderer>().sprite = powerUp.GetComponent<PowerUp>().powerUpSprites[rnd];
            }
 
            // Gdy klocek ma 1 hp
            if (collision.GetComponent<Brick>().life == 1)
            {
                Destroy(collision.gameObject);
                gameManager.score += 10;
                gameManager.bricksCounter--;
            }
            // Gdy ma 2, odejmowane jest mu jedno życia a grafika zmieniona na zniszczoną.
            else if(collision.GetComponent<Brick>().life == 2)
            {
                collision.GetComponent<Brick>().life--;
                collision.GetComponent<Brick>().SetDamageSprite();
            }
        }
    }
}
