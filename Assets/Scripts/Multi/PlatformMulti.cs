﻿using UnityEngine;
using UnityEngine.Networking;

public class PlatformMulti : NetworkBehaviour
{
    public AudioClip laserShot;
    public GameObject laserPrefab;

    public Transform laserShotSpawn1;
    public Transform laserShotSpawn2;

    public bool canShot;

    AudioSource audioSource;

    float speed = 3.5f;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        // Poruszanie platformą

        if (isLocalPlayer)
        {
            float x = Input.GetAxis("Horizontal") * speed;

            transform.Translate(x * Time.deltaTime, 0f, 0f);

            Vector3 clampedPosition = transform.position;

            // Ograniczenie ruchów
            switch (gameObject.name)
            {
                case "BigPlatform(Clone)":
                    clampedPosition.x = Mathf.Clamp(clampedPosition.x, -2.215f, 2.215f);
                    break;

                case "SmallPlatform(Clone)":
                    clampedPosition.x = Mathf.Clamp(clampedPosition.x, -3.141f, 3.141f);
                    break;

                default:
                    clampedPosition.x = Mathf.Clamp(clampedPosition.x, -2.86f, 2.86f);
                    break;
            }
            transform.position = clampedPosition;

            if (gameObject.transform.position.y > -4.237118f) transform.position = new Vector2(transform.position.x, -4.237118f);

            // Gdy gracz podniesie powerUp z działkami, będzie mógł strzelać z nich.
            if (canShot && Input.GetMouseButtonDown(0)) Shot();
        }

    }

    void Shot()
    {
        audioSource.clip = laserShot;
        audioSource.Play();

        Instantiate(laserPrefab, laserShotSpawn1.position, Quaternion.identity);
        Instantiate(laserPrefab, laserShotSpawn2.position, Quaternion.identity);
    }
}
